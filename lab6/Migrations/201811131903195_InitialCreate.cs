namespace lab6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Hospital_HospitalId = c.Int(),
                    })
                .PrimaryKey(t => t.DepartmentId)
                .ForeignKey("dbo.Hospitals", t => t.Hospital_HospitalId)
                .Index(t => t.Hospital_HospitalId);
            
            CreateTable(
                "dbo.Hospitals",
                c => new
                    {
                        HospitalId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.HospitalId);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        StaffId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Age = c.Int(nullable: false),
                        Department_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.StaffId)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentId)
                .Index(t => t.Department_DepartmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Staffs", "Department_DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Departments", "Hospital_HospitalId", "dbo.Hospitals");
            DropIndex("dbo.Staffs", new[] { "Department_DepartmentId" });
            DropIndex("dbo.Departments", new[] { "Hospital_HospitalId" });
            DropTable("dbo.Staffs");
            DropTable("dbo.Hospitals");
            DropTable("dbo.Departments");
        }
    }
}
