﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lab6.Models;

namespace lab6.Controllers
{
    public class HospitalsController : Controller
    {
        // GET: Hospitals
        public ActionResult Index()
        {
            var hospital = new Hospital() { Name = "FER" };
            //  return Content("Hello!");
            // return HttpNotFound();
            return RedirectToAction("Index", "Home", new { page=1, sortBy ="name"});
        }
    }
}