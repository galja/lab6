﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace lab6.Models
{
    public class HospitalContext : DbContext
    {
        public HospitalContext()
            : base("name=HospitalContext")
        {
        }

        public DbSet <Hospital> Hospitals{ get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet <Staff> Staffs { get; set; }
    }
}