﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab6.Models
{
    public class Staff
    {
        public int StaffId { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public int Age { get; set; }
        public virtual Department Department { get; set; }
    }
}