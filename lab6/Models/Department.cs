﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab6.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public String Name { get; set; }
        public virtual Hospital Hospital { get; set; }
        public virtual List<Staff> Staff { get; set; }
    }
}